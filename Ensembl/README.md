Ensembl data was derived from the cDNA and GTF annotations directly from the Ensembl website. Chrom sizes are derived via a combination of R parsing (see below) and the related assembly website (see Human and Mouse sources).

The script `create-tx2gene.R` parses the GTF files and chrom sizes files, searches for matches between the two, and then puts together the chrom names and lengths into a chrom_sizes `data.frame` which is used in combination with the GTF files to create a `TxDb` sqlite database within R. This is then parsed further for the mapping between 'TXNAME' and 'GENEID' for use with downstream gene summarisation tools such as the R package `tximport`. Finally, the txdb and tx2gene objects were saved into this folder as sqlite and csv/rda files, respectively.

Relevant sources:
* https://uswest.ensembl.org/info/data/ftp/index.html

Human:
* https://uswest.ensembl.org/Homo_sapiens/Info/Annotation
* https://www.ebi.ac.uk/ena/data/view/GCA_000001405.25

Mouse:
* http://uswest.ensembl.org/Mus_musculus/Info/Annotation
* https://www.ebi.ac.uk/ena/data/view/GCA_000001635.7 

For the above ebi linka, lick on 'sequence report' (or 'regions'?) link to get sequence lengths for each chromosome/scaffold.


